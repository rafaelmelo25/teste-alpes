# Teste Alpes One - CRUD Simples de uma Agenda de Contatos.

- Teste feito com a view em HTML5, CSS3, Bootstrap 4.

- Usado PHP7 para conectar ao banco de dados MySQL para armazenar, alterar, ver e deletar dados do formulário.

- No banco de dados foram inseridas as informações: id, nome, email, celular.

- Para o formulário funcionar tem que extrair o arquivo crud_alpes que esta na raiz da pasta teste-alpes com o nome (crud_alpes.zip).

- Após extrair o arquivo SQL e importar para o banco de dados, use um servidor apache para rodar o projeto do teste sendo Wampserver, Xampp ou outros.

- Com o servidor rodando e o Banco de Dados configurado, abra o documento teste-alpes.php e altere os dados de acesso do seu Banco de Dados.

- Tudo configurado, acessar o projeto via servidor que se for local, será http://localhost/teste-alpes/teste-alpes.php

- Acessando o formulário, a cada inserção de conteúdo vai aparecer as informações na tabela abaixo a parte Cadastrados, no qual poderá alterar e excluir um cadastro.
