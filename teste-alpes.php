<?php
// Conectando ao banco de dados crud_alpes 
 try {
    $conexao = new PDO("mysql:host=localhost; dbname=crud_alpes", "root", "");
    $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conexao->exec("set names utf8");
} catch (PDOException $erro) {
    echo "Erro na conexão:" . $erro->getMessage();
}

// Verificar se foi enviando dados via POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id = (isset($_POST["id"]) && $_POST["id"] != null) ? $_POST["id"] : "";
    $nome = (isset($_POST["nome"]) && $_POST["nome"] != null) ? $_POST["nome"] : "";
    $email = (isset($_POST["email"]) && $_POST["email"] != null) ? $_POST["email"] : "";
    $celular = (isset($_POST["celular"]) && $_POST["celular"] != null) ? $_POST["celular"] : NULL;
} else if (!isset($id)) {
    // Se não se não foi setado nenhum valor para variável $id
    $id = (isset($_GET["id"]) && $_GET["id"] != null) ? $_GET["id"] : "";
    $nome = NULL;
    $email = NULL;
    $celular = NULL;
}
// salvando novo registro no banco de dados
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "save" && $nome != "") {
    try {
        if ($id != "") {
            $stmt = $conexao->prepare("UPDATE contatos SET nome=?, email=?, celular=? WHERE id = ?");
            $stmt->bindParam(4, $id);
        } else {
            $stmt = $conexao->prepare("INSERT INTO contatos (nome, email, celular) VALUES (?, ?, ?)");
        }
        $stmt->bindParam(1, $nome);
        $stmt->bindParam(2, $email);
        $stmt->bindParam(3, $celular);
         
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                echo "Dados cadastrados com sucesso!";
                $id = null;
                $nome = null;
                $email = null;
                $celular = null;
            } else {
                echo "Erro ao tentar efetivar cadastro";
            }
        } else {
               throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: " . $erro->getMessage();
    }
}

// try {
 
//     $stmt = $conexao->prepare("SELECT * FROM contatos");
 
//         if ($stmt->execute()) {
//             while ($rs = $stmt->fetch(PDO::FETCH_OBJ)) {
//                 echo "<tr>";
//                 echo "<td>".$rs->nome."</td><td>".$rs->email."</td><td>".$rs->celular
//                            ."</td><td><center><a href=\"\">[Alterar]</a>"
//                            ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
//                            ."<a href=\"\">[Excluir]</a></center></td>";
//                 echo "</tr>";
//             }
//         } else {
//             echo "Erro: Não foi possível recuperar os dados do banco de dados";
//         }
// } catch (PDOException $erro) {
//     echo "Erro: ".$erro->getMessage();
// }
//Função para Atualizar o Cadastro
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "upd" && $id != "") {
    try {
        $stmt = $conexao->prepare("SELECT * FROM contatos WHERE id = ?");
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $rs = $stmt->fetch(PDO::FETCH_OBJ);
            $id = $rs->id;
            $nome = $rs->nome;
            $email = $rs->email;
            $celular = $rs->celular;
        } else {
            throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: ".$erro->getMessage();
    }
}
//Função para excluir cadastro
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "del" && $id != "") {
    try {
        $stmt = $conexao->prepare("DELETE FROM contatos WHERE id = ?");
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            echo "Registo foi excluído com êxito";
            $id = null;
        } else {
            throw new PDOException("Erro: Não foi possível executar a declaração sql");
        }
    } catch (PDOException $erro) {
        echo "Erro: ".$erro->getMessage();
    }
}

?>

<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<header>
</header>
<body>
<section class="corpo-form">
<h1>Teste Alpes</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-12">   
                <div class="form form-group">
                <form action="?act=save" method="POST" name="form1" >
                    <h1>Agenda de contatos</h1>
                    <hr>
                    <input class="form-control" type="hidden" name="id" 
                    <?php 
                    // Preenche o id no campo id com um valor "value"
                    if (isset($id) && $id != null || $id != "") {
                        echo "value=\"{$id}\"";
                    }
                    ?> />
                    <div class="col-md-8">
                    Nome:
                    <input class="form-control" type="text" name="nome" placeholder="Nome"
                    <?php
                    // Preenche o nome no campo nome com um valor "value"
                    if (isset($nome) && $nome != null || $nome != ""){
                        echo "value=\"{$nome}\"";
                    }
                    ?>
                    />
                    </div><br>
                    <div class="col-md-8">
                    E-mail:
                    <input class="form-control" type="text" name="email" placeholder="Seu email" <?php
                    // Preenche o email no campo email com um valor "value"
                    if (isset($email) && $email != null || $email != ""){
                        echo "value=\"{$email}\"";
                    }
                    ?>/>
                    </div> <br>
                    <div class="col-md-4">
                    <p>Celular: <input class="form-control" type="text" name="celular" placeholder="Seu Telefone" <?php
                    // Preenche o celular no campo celular com um valor "value"
                    if (isset($celular) && $celular != null || $celular != ""){
                        echo "value=\"{$celular}\"";
                    }
                    ?> /></p>
                    </div>
                    <div class="botoes">
                    <input class="btn btn-outline-secondary" type="submit" value="salvar" />
                    <input class="btn btn-outline-secondary" type="reset" value="Novo" />
                    </div>
                    <hr>
                </form>
                    
                <table border="1" width="100%">
                <h3>Cadastrados</h3>
                    <tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Celular</th>
                        <th>Ações</th>
                    </tr>

                    <?php
    // Bloco que realiza o papel do Read - recupera os dados e apresenta na tela
    try {
        $stmt = $conexao->prepare("SELECT * FROM contatos");
            if ($stmt->execute()) {
                while ($rs = $stmt->fetch(PDO::FETCH_OBJ)) {
                    echo "<tr>";
                    echo "<td>".$rs->nome."</td><td>".$rs->email."</td><td>".$rs->celular
                               ."</td><td><center><a href=\"?act=upd&id=" . $rs->id . "\">[Alterar]</a>"
                               ."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                               ."<a href=\"?act=del&id=" . $rs->id . "\">[Excluir]</a></center></td>";
                    echo "</tr>";
                }
            } else {
                echo "Erro: Não foi possível recuperar os dados do banco de dados";
            }
    } catch (PDOException $erro) {
        echo "Erro: ".$erro->getMessage();
    }
    ?>
                </table>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>